# SXT Light scooter hacks and guides

Collection of links and information regarding SXT Light scooter hacks and maintenance.

## All the experiments were done on this scooter
Type: SXT Light (or E-TWOW S2 BOOSTER)  
Motor: 500w  
Battery: 33.3V 6.5Ah  
Weight: 10.5kg  
Date of purchase: 1/2018  

<img src="pics/scooter_sxt_light.jpeg"  style="height:20%; width:20%" >  
 
   

 ## Local links
 * [Battery replacement](SXT-light-Battery-replacement.md)
 * [Back wheel bearing replacement](SXT-light-bearing-replacement.md)



 ## External link
 * Speed Limit Setting 
   * [Speed Limit Settings | E-TWOW S2 Electric Scooter | PET - YouTube](https://www.youtube.com/watch?v=nUGTM-zPcok)
   * ["Hidden" Speed Limit Setting on E-TWOW Scooter and Availability of Spare Controllers for the German Market : ElectricScooters](https://www.reddit.com/r/ElectricScooters/comments/cjknjy/hidden_speed_limit_setting_on_etwow_scooter_and/)







## Bearing Replacement of the back wheel (SXT light scooter)
After 1100Km driven I started to hear trange sounds from back wheel, probably from bearings, and it turned out that replacement is faily easy. Just put there new bearing, wich turned out is about 5EUR each.  Original bearings were of [6001-2rs](https://duckduckgo.com/?t=ffsb&q=bearing+6001-2rs&ia=web). 

An can gently replace it with small hammer. Just kick gently to the axis.

![Knocking out](pics/bearing-knock-out.jpg "Knocking out the bearing")


and then put it the new bearing onto place, again either it will be posible by hand only, or gently with small hammer.


![Putting new bearing back 1](pics/bearing-replacement-1.jpg "Putting new bearing back 1")
![Putting new bearing back 2](pics/bearing-replacement-2.jpg "Putting new bearing back 2")
![Putting new bearing back 3](pics/bearing-replacement-3.jpg "Putting new bearing back 3")
![Putting new bearing back 4](pics/bearing-replacement-4.jpg "Putting new bearing back 4")
![Putting new bearing back 5](pics/bearing-replacement-5.jpg "Putting new bearing back 5")
![Putting new bearing back 6](pics/bearing-replacement-6.jpg "Putting new bearing back 6")
![Putting new bearing back 7](pics/bearing-replacement-7.jpg "Putting new bearing back 7")

Enjoy a quiet ride again :-)










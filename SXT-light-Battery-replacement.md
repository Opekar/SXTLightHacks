

## Battery replacement (SXT light scooter)

## Disclaimer
Please take care - batteries of this size can be dangerous. If you follow this you do it on your own risk.

## The story

I bought this scooter in 2018, at that time probably lightest scooter on the market, with best performance (500W motor they claim).

Fastforward to in 2020 after about 1000km of driving (~150 charging cycles) the battery started to loose it's capacity,  one day, I wasnt able to finish my usual office commute on a single charge.

Time to search for a new battery.

The SXT original replacement one was >250EUR ([e.g. here](https://www.sxt-scooters.de/en/Spare-parts/SXT-Light/Battery-33V-6-5Ah-Li-Po.html)), Ali express had bateries for similar models for about 1/3 of the price. [here](https://www.aliexpress.com/item/1005001791905181.html?spm=a2g0o.productlist.0.0.213cf29eYATfnx&algo_pvid=df627416-ec93-4596-85f2-9eddf4253bce&algo_expid=df627416-ec93-4596-85f2-9eddf4253bce-22&btsid=0b0a050b16120958040054558e46f4&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_) or [here](https://www.aliexpress.com/i/32915064223.html). The original replacement parts for sxt scooters are expensive with the same reliability 1km costs 25cents just in the battery expenses. Thats not sane or ecological. Maybe RC battery (pouch type) packs have this level of reliability, 18650 li-on cells shall have much better reliability of  around 800 cycles or better. 


After few months I had a battery and it did not fit into the scooter, where the original battery was! :-)

So lets strip it off and 3D print a better vesel for this battery pack.


<img src="pics/model_battery_in_vesel.png"  style="height:20%; width:20%" >

Probably it can be done better - [here is the freecad source](model/battery_vesel_sxt_light.FCStd) for your modification.
It seems that for the 18650 cells the battery space is tight this scooter model but fits.
One bennefit of 18650 cells is that the pack is much shorter then the original on. Means there is room for 1/3 battery capacity increase (by addeding another 9 battery cells in parallel to existing 18.


here you see the ballancer

TODO

 here how it fits

![Putting the battery back](pics/battery-exchange-insert.jpg "Putting the battery back")


Here the printing (hole is drilled through the front part in order the battery can be pulled out easily)



So far I drove about 100km with the new battery it seems to work.

First I was bit hasitant to charge the battery at home, was afraid of fire,  but after few checks it was ok during charging thwre was no temperature increase, so seems safe.

Oreginal battery was Li-on  ( according the voltage the charger was measuring) Now in the spare parts they claim it's Li-Po, so I'm not sure.

I'll be happy if you share with me your battery stories related to SXT Light scooter.

If you have more hacks and guides please send me mail or pull request. 






